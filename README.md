# Cypress Workflow

## Prerequisites:

1.  [Node.js](https://nodejs.org/en/)

## How to use:

1.  Clone the repo

```sh
git clone git@gitlab.com:amrmak7/cypress-workflow.git
```

2.  Install dependencies

```sh
npm install
```

3.  Open cypress

```sh
npm run cypress
```

4.  To commit your changes

```sh
npm run commit
```
